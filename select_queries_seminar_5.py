"""


"""


import psycopg2
conn = psycopg2.connect("host=localhost dbname=trees user=root password=root")

cur = conn.cursor()


# 1. Retrieve all individual tree data for plot no. 3!
cur.execute('SELECT * FROM "tblSingleTree" WHERE "PlotNo" = 3')
all = cur.fetchall()
print(all)

""" 2. Retrieve all individual tree data for plot no. 1 and order them descending by tree no.! """
cur.execute('SELECT * FROM "tblSingleTree" WHERE "PlotNo" = 1 ORDER BY "TreeNo" DESC')
all = cur.fetchall()
print(all)

"""
3. Retrieve the individual tree data of the attributes PlotNo, TreeNo, DBH, and
Height for tree species Common Pine (111) and European Beech and order them
ascending by plot no.!
"""
# Query had to be changed since there is no European Been nor Common Pine on any plot
cur.execute(""" SELECT * FROM "tblSingleTree" 
LEFT JOIN "tblSpecies" ON "tblSingleTree"."SpecCode" = "tblSpecies"."SpecCode"
WHERE "tblSpecies"."Name" IN ('European beech', 'White pine') ORDER BY "PlotNo" ASC """)
all = cur.fetchall()
print(all)

"""
4. Retrieve all individual tree data where diameter at breast height (DBH) is
larger than 20 and less than 60 cm! Order ascending by DBH!
"""
cur.execute(""" SELECT * FROM "tblSingleTree" 
WHERE "tblSingleTree"."DBH" BETWEEN 20 AND 60 ORDER BY "DBH" ASC """)
all = cur.fetchall()
print(all)

"""
5. Retrieve all species data for Pine species! Use the field Name of the respective
table and order ascending by tree species code!

SELECT * FROM "tblSingleTree" 
LEFT JOIN "tblSpecies" ON "tblSingleTree"."SpecCode" = "tblSpecies"."SpecCode"
WHERE "tblSpecies"."Name" LIKE '%pine%' ORDER BY "PlotNo" ASC;


SELECT * FROM "tblSpecies" WHERE "tblSpecies"."Name" LIKE '%pine%';

-- only return entries for 112 because there is no 112, 113
SELECT * FROM "tblSingleTree" WHERE "SpecCode" IN (111,112,113)

"""
cur.execute(""" SELECT * FROM "tblSingleTree" 
LEFT JOIN "tblSpecies" ON "tblSingleTree"."SpecCode" = "tblSpecies"."SpecCode"
WHERE "tblSpecies"."Name" LIKE '%pine%' ORDER BY "PlotNo" ASC """)
all = cur.fetchall()
print(all)

"""
6. Retrieve all species data for Pine species! Use the field LatinName of the
respective table and order ascending by tree species code!
"""
# is like the task 5
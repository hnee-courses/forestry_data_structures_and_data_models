### see https://www.dataquest.io/blog/loading-data-into-postgres/

import csv

import psycopg2
# load the psycopg extras module
import psycopg2.extras
import pandas as pd

from exam.data_selection import run_select
from exam.data_selection_v2 import run_select_v2
from exam.exam_functions import DataHelper


def import_data_exam():
    print(f"== starting import of data ==")
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur2 = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    cur = conn.cursor()
    dh = DataHelper()

    df_federal_states = dh.get_exam_data()["datatable"][
        ["federal_state"]]  ## TODO this should be reduced, otherwise you try to insert 45 rows

    # INSERT INGORE the states now
    data_dict = df_federal_states.to_dict(orient="records")
    cur.executemany("""INSERT INTO public.federal_state(federal_state) 
        VALUES (%(federal_state)s) ON CONFLICT (federal_state) DO NOTHING ;""",
                    data_dict)
    conn.commit()

    ## insert the AOI now
    df_aoi = dh.get_exam_data()["datatable"][["aoi"]]
    data_dict = df_aoi.to_dict(orient="records")
    cur.executemany("""INSERT INTO public.aoi(aoi) 
            VALUES (%(aoi)s) ON CONFLICT (aoi) DO NOTHING ;""",
                    data_dict)
    conn.commit()

    ## insert the campaigns now
    df_aoi = dh.get_exam_data()["datatable"][["campaign_no", "year_of_sampling", "sampling_type"]]
    data_dict = df_aoi.to_dict(orient="records")
    cur.executemany("""INSERT INTO public.campaigns(campaign_no, year_of_sampling, sampling_type) 
            VALUES (
            %(campaign_no)s,%(year_of_sampling)s,%(sampling_type)s
            ) ON CONFLICT (campaign_no) DO NOTHING ;""",
                    data_dict)
    conn.commit()

    ## insert the soil units now
    df_aoi = dh.get_exam_data()["datatable"][["soil_unit"]]
    data_dict = df_aoi.to_dict(orient="records")
    cur.executemany("""INSERT INTO public.soil_unit(soil_unit) 
            VALUES (
            %(soil_unit)s
            ) ON CONFLICT (soil_unit) DO NOTHING ;""",
                    data_dict)
    conn.commit()

    cur2.execute('SELECT * FROM public.soil_unit')
    dict_soil_unit = cur2.fetchall()
    df_soil_unit = pd.DataFrame([dict(x) for x in dict_soil_unit])
    # do a left join to get the soil unit ids in the dataframe
    df_sample_points = pd.merge(
        dh.get_exam_data()["datatable"][["sample_point_no", "x_coord", "y_coord", "soil_unit"]],
        df_soil_unit,
        how="left",
        on="soil_unit",
    ).drop("soil_unit", axis=1)

    ## insert the sampling_point now
    data_dict = df_sample_points.to_dict(orient="records")
    cur.executemany("""INSERT INTO public.sample_points(sample_point_no, x_coord, y_coord, soil_unit_id) 
            VALUES (
            %(sample_point_no)s, %(x_coord)s, %(y_coord)s, %(soil_unit_id)s
            ) ON CONFLICT (sample_point_no) DO NOTHING ;""",
                    data_dict)
    conn.commit()

    """
    enrich the measurements with ids from postgres now
    """
    df_measurements = dh.get_exam_data()["datatable"][["id_corg_measurement", "federal_state", "aoi", "sample_point_no",
                                                       "campaign_no", "corg_content", "bd1"
                                                       ]]

    ## now get the states
    cur2.execute('SELECT * FROM public.federal_state')
    dict_states = cur2.fetchall()
    df_states = pd.DataFrame([dict(x) for x in dict_states])
    # do a left join to get the federal state id in the dataframe
    df_measurements = pd.merge(
        df_measurements,
        df_states,
        how="left",
        on="federal_state",
    ).drop("federal_state", axis=1)

    ## get the aois
    cur2.execute('SELECT aoi_id, aoi FROM public.aoi')
    dict_aoi = cur2.fetchall()
    df_aoi = pd.DataFrame([dict(x) for x in dict_aoi])

    # do a left join to get the federal state id in the dataframe
    df_measurements = pd.merge(
        df_measurements,
        df_aoi,
        how="left",
        on="aoi",
    ).drop("aoi", axis=1)

    data_dict = df_measurements.to_dict(orient="records")
    cur.executemany("""INSERT INTO public.measurements(
                id_corg_measurement, federal_state_id, aoi_id, sample_point_no, campaign_no,
                corg_content, bd1
                ) VALUES (
                %(id_corg_measurement)s, %(federal_state_id)s, %(aoi_id)s, %(sample_point_no)s, %(campaign_no)s,
                %(corg_content)s, %(bd1)s 
    
    ) 
    ON CONFLICT (id_corg_measurement) DO UPDATE SET 
    corg_content = %(corg_content)s, bd1 = %(bd1)s
    ;""", data_dict)

    conn.commit()
    print(f"== done with import of data ==")


if __name__ == '__main__':
    from db_migration_exam import migration, data_migration

    # setup the database schema
    migration()
    # import the data from the given excel file
    import_data_exam()

    run_select()
    data_migration()
    # execute various select queries

    run_select_v2()

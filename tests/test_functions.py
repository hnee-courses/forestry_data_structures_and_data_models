"""
Test Class for different used functions to ensure all work as they are exptected

@author: Christian Winkelmann
"""
import unittest

from exam.exam_functions import DataHelper


class TreeVolumePredictorTests(unittest.TestCase):
    def setUp(self):
        self.tve = {
            "metadata": {
                "dbh": {
                    "description": "diameter at breast height (1.3m) in centimeters "
                },
                "age": {"description": "Tree Age in years "},
                "h": {"description": "Tree height in meters"},
                "v": {"description": "Volume of tree in cubic decimeters"},
            },
            "data": [
                {"dbh": 26.5, "age": 100, "h": 30, "v": 928.0},
            ],
        }
        # self.longMessage = True  # enable long message at testing
        self.dh = DataHelper()

    def tearDown(self):
        """
        tear down anything we have setup.
        :return:
        """
        pass


    def test_get_exam_data(self):
        """
        """
        df_corgdb = self.dh.get_exam_data()

    def test_get_corg_db_grouped(self):
        """
        """
        df_corgdb = self.dh.get_corg_db_grouped()






if __name__ == "__main__":
    unittest.main()

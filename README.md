# Forestry data structures and data models

[Course Moodle Room](https://lms.hnee.de/mod/bigbluebuttonbn/view.php?id=2041).
 
## Install Docker
https://towardsdatascience.com/how-to-run-postgresql-and-pgadmin-using-docker-3a6a8ae918b5

## Start the docker containers
```shell 
docker-compose up
```

## Open pgAdmin in your browser
http://localhost:5050/browser/#

- username: admin@admin.com
- password: root

In the pgAdmin gui connect to the server which was started already through docker compose. 
- Hostname: pg_container
- usnername: root
- password: root

### install the dependencies using conda or pip
have a look at requirements.txt to install the postgres client and other dependencies

```shell
## depending on your environment this might do your job 
pip install -r requirements.txt
```

Now run the python scrip "run_exam.py". This will create the database schema, import data and execute again. WARNING, In Windows the paths might look different and the exam data has to copied in place first.

``` 
python3 run_exam.py
```

Go to the pgAdmin gui http://localhost:5050/browser/# and check for the new tables.
![tables.png](doc/tables.png)


After you are done, stop the containers
```
## shut down docker container
docker-compose down 

## delete containers
docker systen prune

```


### see https://www.dataquest.io/blog/loading-data-into-postgres/

import csv

import psycopg2
conn = psycopg2.connect("host=localhost dbname=trees user=root password=root")

cur = conn.cursor()

## import tblCountry
with open('data/tblCountry.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader) # Skip the header row.
    for row in reader:
        cur.execute(
        'INSERT INTO trees.public."tblCountry" VALUES (%s, %s) ON CONFLICT ON CONSTRAINT "tblCountry_pkey" DO NOTHING',
        row
    )
conn.commit()
cur.execute('SELECT * FROM trees.public."tblCountry"')
all = cur.fetchall()

## import tblPlot
with open('data/tblPlot.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader) # Skip the header row.
    for row in reader:
        cur.execute(
        'INSERT INTO trees.public."tblPlot" ("PlotNo","UtmZone","Easting","Northing","Comment","IDCountry") VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT ON CONSTRAINT "tblPlot_pkey" DO NOTHING',
        row
    )
conn.commit()
cur.execute('SELECT * FROM trees.public."tblPlot"')
all = cur.fetchall()

## import tblSpecies
with open('data/tblSpecies.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader) # Skip the header row.
    for row in reader:
        cur.execute(
        'INSERT INTO trees.public."tblSpecies" VALUES (%s, %s,%s, %s) ON CONFLICT ON CONSTRAINT "tblSpecies_pkey" DO NOTHING',
        row
    )
conn.commit()
cur.execute('SELECT * FROM trees.public."tblSpecies"')
all = cur.fetchall()


## import tblSingleTree
with open('data/tblSingleTree.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader) # Skip the header row.
    for row in reader:
        cur.execute(
        'INSERT INTO trees.public."tblSingleTree" ("PlotNo","TreeNo","SpecCode","DBH","Height","Easting_local","Northing_local") '
        'VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT ON CONSTRAINT "uq_pk" DO NOTHING',
        row
    )
conn.commit()
cur.execute('SELECT * FROM trees.public."tblSingleTree"')
all = cur.fetchall()
print(all)


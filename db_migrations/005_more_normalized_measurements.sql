

CREATE TABLE IF NOT EXISTS public.measurement_type
(
    measurement_type_id SERIAL,
    measurement_type character varying(24),

    PRIMARY KEY (measurement_type_id),
    CONSTRAINT uq_measurement_type_measurement_type UNIQUE (measurement_type)
);


-- ###########################################################
-- very flexible measurements_v2 fact table
-- ###########################################################
CREATE TABLE IF NOT EXISTS public.measurements_v2
(
    id_measurement integer NOT NULL,
    measurement_type_id integer NOT NULL,
    -- sample_point_no integer,
    measurement_timestamp timestamp with time zone DEFAULT now(),

    content numeric NOT NULL,
-- TODO define some sort of unit
    PRIMARY KEY (id_measurement, measurement_type_id)
);


CREATE TABLE IF NOT EXISTS public.measurement_campaign_mapping
(
    id_measurement integer NOT NULL,
    campaign_no integer NOT NULL,

    PRIMARY KEY (id_measurement),
    UNIQUE (id_measurement, campaign_no)

);
COMMENT ON TABLE public.measurement_campaign_mapping
    IS 'map each measurement to a campaign';

CREATE TABLE IF NOT EXISTS public.measurement_sample_point_mapping
(
    id_measurement integer NOT NULL,
    sample_point_no integer NOT NULL,

    PRIMARY KEY (id_measurement),
    UNIQUE (id_measurement, sample_point_no)

);
COMMENT ON TABLE public.measurement_campaign_mapping
    IS 'map each measurement to a campaign';



CREATE TABLE IF NOT EXISTS public.campaigns_v2
(
    campaign_no integer NOT NULL,
    federal_state_id integer NOT NULL,
    aoi_id integer NOT NULL,

    year_of_sampling integer,
    sampling_type sampling_type,

    PRIMARY KEY (campaign_no, federal_state_id, aoi_id),

    -- FOREIGN KEY (measurement_type_id) REFERENCES public.measurement_type (measurement_type_id) MATCH SIMPLE

    CONSTRAINT uq_campaign_no UNIQUE (campaign_no)
);





-- Add the foreign keys
ALTER TABLE public.measurements_v2
    ADD FOREIGN KEY (measurement_type_id)
    REFERENCES public.measurement_type (measurement_type_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

ALTER TABLE public.measurements_v2
    ADD FOREIGN KEY (id_measurement)
    REFERENCES public.measurement_campaign_mapping (id_measurement) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

ALTER TABLE public.measurements_v2
    ADD FOREIGN KEY (id_measurement)
    REFERENCES public.measurement_sample_point_mapping (id_measurement) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;


-- TODO ON DELETE CASCADE BUT NOT

INSERT INTO public.measurement_type (measurement_type) VALUES ('corg'),
                                                              ('ph'),
                                                              ('nmin'),

                                                               ('bd1'), ('bd2'), ('bd3'),
                                                               ('fc1'), ('fc2'), ('fc3'),
                                                               ('pwp1'), ('pwp2'), ('pwp3')
                                                               ON CONFLICT (measurement_type) DO NOTHING;



ALTER TABLE IF EXISTS public.campaigns_v2
    ADD FOREIGN KEY (aoi_id)
    REFERENCES public.aoi (aoi_id) MATCH SIMPLE;

ALTER TABLE IF EXISTS public.campaigns_v2
    ADD FOREIGN KEY (federal_state_id)
    REFERENCES public.federal_state (federal_state_id) MATCH SIMPLE;

ALTER TABLE IF EXISTS public.measurement_campaign_mapping
    ADD FOREIGN KEY (campaign_no)
    REFERENCES public.campaigns_v2 (campaign_no) MATCH SIMPLE;

ALTER TABLE IF EXISTS public.measurement_sample_point_mapping
    ADD FOREIGN KEY (sample_point_no)
    REFERENCES public.sample_points (sample_point_no) MATCH SIMPLE;
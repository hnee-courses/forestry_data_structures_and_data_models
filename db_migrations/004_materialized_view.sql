
BEGIN;


CREATE MATERIALIZED VIEW public.measurements_mv
AS
 SELECT
        federal_state.federal_state,
        aoi.aoi,
        measurements.id_corg_measurement,
        sample_points.y_coord,
        sample_points.x_coord,
        sample_points.sample_point_no,
        sample_points.soil_unit,
        campaigns.campaign_no,
        campaigns.year_of_sampling,
        campaigns.sampling_type,
        -- String("Corg") AS measurement,
        -- String("Corg") AS soil_unit_definition,
        measurements.corg_content,
        measurements.bd1,
        measurements.bd2,
        measurements.bd3,
        measurements.fc1,
        measurements.fc2,
        measurements.fc3,
        measurements.pwp1,
        measurements.pwp2,
        measurements.pwp3

 FROM measurements

        LEFT JOIN campaigns ON measurements.campaign_no = campaigns.campaign_no
        LEFT JOIN aoi ON measurements.aoi_id = aoi.aoi_id
        LEFT JOIN federal_state ON measurements.federal_state_id = federal_state.federal_state_id
        LEFT JOIN (
            SELECT * FROM sample_points
            LEFT JOIN soil_unit ON sample_points.soil_unit_id = soil_unit.soil_unit_id
        ) sample_points ON measurements.sample_point_no = sample_points.sample_point_no
WITH NO DATA;




END;
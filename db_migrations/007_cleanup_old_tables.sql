-- Now we can delete the old outdated tables
BEGIN;

DROP MATERIALIZED VIEW public.measurements_mv;
DROP TABLE measurements;
DROP TABLE campaigns;


END;
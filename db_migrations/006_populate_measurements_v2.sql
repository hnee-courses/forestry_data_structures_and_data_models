
BEGIN;


-- Fill the campaigns_v2 table
INSERT INTO public.campaigns_v2
 (federal_state_id, aoi_id, campaign_no, year_of_sampling, sampling_type)


SELECT
		federal_state_id,
        aoi_id,
        campaign_no,
        year_of_sampling,
        sampling_type
		FROM
(
SELECT
DISTINCT(
        federal_state.federal_state_id,
        aoi.aoi_id,
        campaigns.campaign_no,
        campaigns.year_of_sampling,
        campaigns.sampling_type ),
		federal_state.federal_state_id,
        aoi.aoi_id,
        campaigns.campaign_no,
        campaigns.year_of_sampling,
        campaigns.sampling_type

 FROM measurements

        LEFT JOIN campaigns ON measurements.campaign_no = campaigns.campaign_no
        LEFT JOIN aoi ON measurements.aoi_id = aoi.aoi_id
        LEFT JOIN federal_state ON measurements.federal_state_id = federal_state.federal_state_id
        LEFT JOIN (
            SELECT * FROM sample_points
            LEFT JOIN soil_unit ON sample_points.soil_unit_id = soil_unit.soil_unit_id
        ) sample_points ON measurements.sample_point_no = sample_points.sample_point_no

) a;


INSERT INTO public.measurement_campaign_mapping (id_measurement, campaign_no)
SELECT
       id_corg_measurement, campaign_no
FROM public.measurements;



INSERT INTO public.measurement_sample_point_mapping (id_measurement, sample_point_no)
SELECT
       id_corg_measurement, sample_point_no
FROM public.measurements;


-- populate with corg data
INSERT INTO public.measurements_v2 (id_measurement, measurement_type_id, content)
SELECT
       id_corg_measurement,
       measurement_type.measurement_type_id,
       corg_content

FROM public.measurements
LEFT JOIN measurement_type ON measurement_type = 'corg';


-- populate the table from the old table
INSERT INTO public.measurements_v2 (id_measurement, measurement_type_id, content)
SELECT
       id_corg_measurement,
       measurement_type.measurement_type_id,
       bd1

FROM public.measurements
LEFT JOIN measurement_type ON measurement_type = 'bd1';


-- necessary for crosstab
CREATE EXTENSION IF NOT EXISTS tablefunc;






---- TODO THis is not ready yet because the sample point is missing
CREATE MATERIALIZED VIEW public.measurements_v3_mv
AS
    SELECT
    measurement_campaign_mapping.id_measurement,
    measurement_sample_point_mapping.sample_point_no,
    sample_points.x_coord,
    sample_points.y_coord,
    -- sample_points.soil_unit_id,
    soil_unit.soil_unit,
    federal_state.federal_state,
    aoi.aoi,
    campaigns_v2.year_of_sampling,

    content_corg,
    content_bd1

    FROM (
    SELECT *
        FROM crosstab(
          $$SELECT measurements_v2.id_measurement, measurement_type.measurement_type, measurements_v2.content
            FROM public.measurements_v2
            LEFT JOIN measurement_type ON measurement_type.measurement_type_id = measurements_v2.measurement_type_id
            WHERE measurement_type IN ('corg', 'bd1')
            ORDER  BY 1, CASE measurement_type WHEN 'corg' THEN 1 WHEN 'bd1' THEN 2 ELSE 3 END; $$ )
        AS ct(id_measurement integer, content_corg numeric, content_bd1 numeric)
    ) a
        LEFT JOIN measurement_campaign_mapping ON a.id_measurement = measurement_campaign_mapping.id_measurement
        LEFT JOIN campaigns_v2 ON campaigns_v2.campaign_no = measurement_campaign_mapping.campaign_no

        LEFT JOIN measurement_sample_point_mapping ON a.id_measurement = measurement_sample_point_mapping.id_measurement
        LEFT JOIN sample_points ON sample_points.sample_point_no = measurement_sample_point_mapping.sample_point_no

        lEFT JOIN soil_unit ON soil_unit.soil_unit_id = sample_points.soil_unit_id

        LEFT JOIN federal_state ON federal_state.federal_state_id = campaigns_v2.federal_state_id
        LEFT JOIN aoi ON aoi.aoi_id = campaigns_v2.aoi_id


    ----
WITH DATA;


REFRESH MATERIALIZED VIEW public.measurements_mv;
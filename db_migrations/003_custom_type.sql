

DROP TABLE IF EXISTS public."measurements" CASCADE;
DROP TABLE IF EXISTS public."measurements_v2" CASCADE;

DROP TABLE IF EXISTS public."campaigns" CASCADE;
DROP TABLE IF EXISTS public."campaigns_v2" CASCADE;
DROP TABLE IF EXISTS public."sample_points" CASCADE;
DROP TABLE IF EXISTS public."federal_state" CASCADE;
DROP TABLE IF EXISTS public."aoi" CASCADE;
DROP TABLE IF EXISTS public."soil_unit" CASCADE;
DROP TABLE IF EXISTS public."measurement_type" CASCADE;
DROP TABLE IF EXISTS public."measurement_campaign_mapping" CASCADE;
DROP TABLE IF EXISTS public."measurement_sample_point_mapping" CASCADE;


DROP TYPE IF EXISTS sampling_type;

CREATE TYPE sampling_type AS ENUM ('raster sampling', 'irregular sampling', 'permanent sampling');
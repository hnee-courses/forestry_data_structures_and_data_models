## TODO take the migrations statements and dump it into the db
# https://www.dataquest.io/blog/loading-data-into-postgres/
"""
create the database tables. If they already exists probably nothing happens. So delete them manually before


"""
from time import sleep


def migration():
    """
    setup the database schema
    :return:
    """
    import psycopg2
    import psycopg2.errors
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur = conn.cursor()

    try:
        # reset database and create the custom type

        migration = "./db_migrations/003_1_drop_view.sql"
        with open(migration, 'r') as f:
            create_query = f.read()
            cur.execute(create_query)
        conn.commit()

        migration = "./db_migrations/003_custom_type.sql"
        with open(migration, 'r') as f:
            create_query = f.read()
            cur.execute(create_query)
        conn.commit()


    except psycopg2.errors.DuplicateObject:
        """ naah thats ok """
        pass
    except psycopg2.errors.DependentObjectsStillExist as e:
        print("cannot drop table measurements because other objects")
        print(e)
    finally:
        conn.commit()
    conn.close()



    schemas = [
        #"./db_migrations/001_table_defintion.sql",
        #"./db_migrations/002_foreign_key.sql",
        "./db_migrations/003_table_defintion_exam.sql",
        "./db_migrations/004_materialized_view.sql",
        "./db_migrations/005_more_normalized_measurements.sql",
    ]

    try:
        conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
        cur = conn.cursor()
        for schema in schemas:
            with open(schema, 'r') as f:
                create_query = f.read()
                cur.execute(create_query)
            print(f"execute migration: {schema}")
            conn.commit()

    except psycopg2.errors.DuplicateTable as e:
        print(schema)
        print("DuplicateTable")
        print(create_query)
        raise e

    except psycopg2.errors.InvalidForeignKey as e:
        print(schema)
        print("InvalidForeignKey")
        print(create_query)
        raise e


def data_migration():
    """
    setup the database schema
    :return:
    """
    import psycopg2
    import psycopg2.errors
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur = conn.cursor()


    schemas = [
        "./db_migrations/006_populate_measurements_v2.sql",
        "./db_migrations/007_cleanup_old_tables.sql",
    ]

    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur = conn.cursor()
    for schema in schemas:
        with open(schema, 'r') as f:
            create_query = f.read()
            cur.execute(create_query)
        print(f"execute migration: {schema}")
        conn.commit()

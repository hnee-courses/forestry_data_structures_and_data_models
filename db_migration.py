## TODO take the migrations statements and dump it into the db
# https://www.dataquest.io/blog/loading-data-into-postgres/
"""
create the database tables. If they already exists probably nothing happens. So delete them manually before


"""
import psycopg2
conn = psycopg2.connect("host=localhost dbname=trees user=root password=root")
cur = conn.cursor()

schemas = [
    #"./db_migrations/001_table_defintion.sql",
    #"./db_migrations/002_foreign_key.sql",
    "./db_migrations/003_table_defintion_exam.sql",
]

for schema in schemas:

    ## import tblSpecies
    with open(schema, 'r') as f:
        create_query = f.read()
        cur.execute(create_query)
    conn.commit()
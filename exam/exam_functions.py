import os
from pathlib import Path
import pandas as pd

class DataHelper(object):
    """
    Helper Class to retrieve dataset
    """

    datatable_exam_path = Path(os.path.abspath(__file__)).parent.joinpath(
        "Datatable_Exam.xlsx"
    )

    def get_exam_data(self) -> pd.DataFrame:
        """
        read the excel sheet and transform the data into something usefull
        :return:
        """
        with open(self.datatable_exam_path, "r") as f:
            xl_file = pd.ExcelFile(self.datatable_exam_path)

            df_data_table = xl_file.parse("Datatable")
            df_data_table.columns = df_data_table.columns\
                .str.replace(" ", "_").str.lower()\
                .str.replace(".", "")
            df_meta_table = xl_file.parse("Metainformation")

            # df_data_table.nunique()

            dfs = {
                "datatable" : df_data_table,
                "metainformation" : df_meta_table
            }

            return dfs


import json
from pathlib import Path

import psycopg2.extras
import pandas as pd
import psycopg2
from loguru import logger
import matplotlib.pyplot as plt

def run_select_v2():
    """

    :return:
    """
    select_compare_on()
    compare_soil_type()


def select_compare_on():
    """
    get a timeseries comparing sample points per year

    :return:
    """
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur2 = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    print("== build a dataframe which looks pretty much the same than the orginial data from excel ==")
    cur2.execute("""
    SELECT
    -- a.id_measurement,
    measurement_sample_point_mapping.sample_point_no,
    campaigns_v2.year_of_sampling,

    content_corg
    -- content_bd1

    FROM (
    SELECT *
        FROM crosstab(
          $$SELECT measurements_v2.id_measurement, measurement_type.measurement_type, measurements_v2.content
            FROM public.measurements_v2
            LEFT JOIN measurement_type ON measurement_type.measurement_type_id = measurements_v2.measurement_type_id
            WHERE measurement_type IN ('corg', 'bd1')
            ORDER  BY 1, CASE measurement_type WHEN 'corg' THEN 1 WHEN 'bd1' THEN 2 ELSE 3 END; $$ )
        AS ct(id_measurement integer, content_corg numeric, content_bd1 numeric)
    ) a
        LEFT JOIN measurement_campaign_mapping ON a.id_measurement = measurement_campaign_mapping.id_measurement
        LEFT JOIN campaigns_v2 ON campaigns_v2.campaign_no = measurement_campaign_mapping.campaign_no

        LEFT JOIN measurement_sample_point_mapping ON a.id_measurement = measurement_sample_point_mapping.id_measurement
        LEFT JOIN sample_points ON sample_points.sample_point_no = measurement_sample_point_mapping.sample_point_no
    """)
    all = cur2.fetchall()
    df_all = pd.DataFrame([dict(x) for x in all])

    df_all["content_corg"] = pd.to_numeric(df_all["content_corg"], downcast="float")
    df_pivot = df_all.pivot(index='year_of_sampling', columns='sample_point_no', values='content_corg')
    print(df_pivot.head())
    df_pivot.plot(xlabel="year", ylabel="organic carbon in soil [mg/kg]")

    plt.savefig("organic_carbon_in_soil.jpg")
    plt.show()
    conn.close()



def compare_soil_type():
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur2 = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    print("== build a dataframe which looks pretty much the same than the orginial data from excel ==")
    cur2.execute("""
    SELECT
    soil_unit.soil_unit,
    ROUND(AVG(content_corg::numeric), 2) AS avg_content_corg
    
    FROM (
    SELECT *
        FROM crosstab(
          $$SELECT measurements_v2.id_measurement, measurement_type.measurement_type, measurements_v2.content
            FROM public.measurements_v2
            LEFT JOIN measurement_type ON measurement_type.measurement_type_id = measurements_v2.measurement_type_id
            WHERE measurement_type IN ('corg', 'bd1')
            ORDER  BY 1, CASE measurement_type WHEN 'corg' THEN 1 WHEN 'bd1' THEN 2 ELSE 3 END; $$ )
        AS ct(id_measurement integer, content_corg numeric, content_bd1 numeric)
    ) a
        -- LEFT JOIN measurement_campaign_mapping ON a.id_measurement = measurement_campaign_mapping.id_measurement
        -- LEFT JOIN campaigns_v2 ON campaigns_v2.campaign_no = measurement_campaign_mapping.campaign_no

        LEFT JOIN measurement_sample_point_mapping ON a.id_measurement = measurement_sample_point_mapping.id_measurement
        LEFT JOIN sample_points ON sample_points.sample_point_no = measurement_sample_point_mapping.sample_point_no

        lEFT JOIN soil_unit ON soil_unit.soil_unit_id = sample_points.soil_unit_id

        -- LEFT JOIN federal_state ON federal_state.federal_state_id = campaigns_v2.federal_state_id
        -- LEFT JOIN aoi ON aoi.aoi_id = campaigns_v2.aoi_id
		
	GROUP BY soil_unit
	ORDER BY avg_content_corg
    """)
    all = cur2.fetchall()
    df_all = pd.DataFrame([dict(x) for x in all])

    df_all["content_corg"] = pd.to_numeric(df_all["avg_content_corg"], downcast="float")

    df_all.plot.bar(x="soil_unit", xlabel="soil type", ylabel="avg organic carbon in soil [mg/kg]", legend=False)

    plt.savefig("organic_carbon_by_soil.jpg")
    plt.show()
    conn.close()



if __name__ == '__main__':

    run_select_v2()
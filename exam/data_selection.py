import json
from pathlib import Path

import psycopg2.extras
import pandas as pd
import psycopg2
from loguru import logger

def run_select():
    """

    :return:
    """
    select_all()
    select_average_corg_by_soil()
    select_average_corg_by_soil_and_year()
    select_on_materialized_view()



def select_all():

    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur2 = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    print("== build a dataframe which looks pretty much the same than the orginial data from excel ==")
    cur2.execute("""
    SELECT * FROM measurements 

        LEFT JOIN campaigns ON measurements.campaign_no = campaigns.campaign_no
        LEFT JOIN aoi ON measurements.aoi_id = aoi.aoi_id
        LEFT JOIN federal_state ON measurements.federal_state_id = federal_state.federal_state_id
        LEFT JOIN (
            SELECT * FROM sample_points
            LEFT JOIN soil_unit ON sample_points.soil_unit_id = soil_unit.soil_unit_id
        ) n2 ON measurements.sample_point_no = n2.sample_point_no
    """)
    all = cur2.fetchall()
    df_all = pd.DataFrame([dict(x) for x in all])
    print(df_all.head())

    conn.close()


def select_average_corg_by_soil():
    """
    get the corg values
    :return:
    """
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur2 = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur2.execute("""
    SELECT 
        soil_unit,
        ROUND(AVG(corg_content)::numeric, 2 ) AS avg_corg_content,
        ROUND(AVG(bd1), 2 ) AS avg_bd1

        FROM measurements 

            LEFT JOIN (
                SELECT * FROM sample_points
                LEFT JOIN soil_unit ON sample_points.soil_unit_id = soil_unit.soil_unit_id
            ) n2 ON measurements.sample_point_no = n2.sample_point_no
        GROUP BY n2.soil_unit
        ORDER BY n2.soil_unit ASC;
    """)
    all = cur2.fetchall()
    df_all = pd.DataFrame([dict(x) for x in all])

    print(f" calculated averages\n {df_all}")

    conn.close()

def select_average_corg_by_soil_and_year():
    """
    get the corg values by soil and year
    :return:
    """
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur2 = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur2.execute("""
    SELECT 
        year_of_sampling,
        soil_unit,
        
        ROUND(AVG(corg_content)::numeric, 2 ) AS avg_corg_content,
        ROUND(AVG(bd1), 2 ) AS avg_bd1

        FROM measurements 
            LEFT JOIN campaigns ON measurements.campaign_no = campaigns.campaign_no
            LEFT JOIN (
                SELECT * FROM sample_points
                LEFT JOIN soil_unit ON sample_points.soil_unit_id = soil_unit.soil_unit_id
            ) n2 ON measurements.sample_point_no = n2.sample_point_no
        GROUP BY n2.soil_unit, year_of_sampling
        ORDER BY year_of_sampling ASC, n2.soil_unit ASC;
    """)
    all = cur2.fetchall()
    df_all = pd.DataFrame([dict(x) for x in all])

    print(f" calculated averages by soil type and year \n {df_all}")

    conn.close()


def select_on_materialized_view():
    """
    try out the materialized view feature
    :return:
    """
    conn = psycopg2.connect("host=localhost dbname=test_db user=root password=root")
    cur2 = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur2.execute("""REFRESH MATERIALIZED VIEW measurements_mv""")
    cur2.execute("""
    SELECT
            *
        FROM
            measurements_mv;
        ;
    """)
    all = cur2.fetchall()
    df_all = pd.DataFrame([dict(x) for x in all])

    print(f" rebuilt the whole dataframe from a materialzed view \n {df_all}")

    conn.close()
    with open(Path("./data_as_it_was_before.json"), 'w') as f:

        json.dump(df_all.to_json(orient="records"), f)

if __name__ == '__main__':

    run_select()
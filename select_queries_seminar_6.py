"""
1. Retrieve all individual tree data together with all tree species data! Use the
Natural Join command for that!
2. Retrieve the plot no. and coordinates for each country. First, display only
matching results. Secondly, include also not matching results!
3. Retrieve the individual tree data PlotNo, TreeNo, DBH, and Height together
with the tree species name for plot no. 17! Use the Inner Join command for
that!
4. Calculate the count, average, minimum, and maximum for the attribute tree
height grouped by the tree species name and the plot number! Sort the query
outcome ascending by plot number!
5. Add a new column to the table containing the individual tree data. Name the
column BasalArea and define the field data type as numeric with a maximum
of two decimals. Update the table by setting the values in the new column
BasalArea to 3.14159/4 * DBH.
6. Optional: Improve the database structure by adding a new field IDPlot to
tblPlot. Update the primary key to the new field. Adapt the field PlotNo to a
foreign key field of the primary key field PlotNo in tblSingleTree.


"""


import psycopg2
conn = psycopg2.connect("host=localhost dbname=trees user=root password=root")

cur = conn.cursor()

cur.execute('SELECT * FROM trees.public."tblCountry"')
all = cur.fetchall()